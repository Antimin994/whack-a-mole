const gameField = document.getElementById("newGame");
const inputTimer = document.getElementById('submit');
let countTd = 0;
let currentTr = null;
let currentTd = null;
let counter = 0;
let step = 0;
let levelInterval;
let currentEl = null;
let countArr = new Array();
let playerCount = 0;
let computerCount = 0;

for (let i = 0; i < 10; i++) {
  gameField.insertAdjacentHTML('beforeend', `<tr id="tr-${i}"></tr>`);
  currentTr = document.getElementById(`tr-${i}`);
  for (let j = 0; j < 10; j++) {
    countTd = i*10+j+1;
    currentTr.insertAdjacentHTML('beforeend', `<td id="td-${countTd}"></td>`);
    currentTd = document.getElementById(`td-${countTd}`);
  }
}

function chooseRect(timer) {
  clearTimeout(levelInterval);
  counter = Math.ceil(Math.random()*100);
  if (countArr.includes(counter) === true) {
    return chooseRect(timer);
  }
  currentEl = document.getElementById(`td-${counter}`);
  currentEl.classList.add("blue");
  currentEl.addEventListener('click', (event) => {
  if ((event.target === currentEl) && !currentEl.classList.contains('green') && !currentEl.classList.contains('red')) {
      currentEl.classList.add("green");
      playerCount++;
      document.getElementById("player-count").innerText = playerCount;
    };
    
  });

  countArr.push(counter)
  
  levelInterval = setTimeout(() => {
    
    currentEl.classList.remove("blue");
    
    if (!currentEl.classList.contains('green')) {
      currentEl.classList.add("red");
      computerCount++;
      document.getElementById("computer-count").innerText = computerCount;
    } 
    countArr.length < 100 ? chooseRect(timer) : showResults();
  }, timer)

}

document.getElementById('submit').onclick = function(event) {
  startGame();
  var radios = document.getElementsByName('level');
  for (var radio of radios)
  {
      if (radio.checked) {
          switch (radio.value) {
            case 'easy':
              step = 1500
              break;
              case 'medium':
              step = 1000
              break;
              case 'hard':
              step = 500
              break;
              default:
              break;
          }
      }
  }

  chooseRect(step);

}

function showResults() {
  if (playerCount > computerCount) {
    document.querySelector(".show-winner").textContent = "Ви переможець!";
  } else if (playerCount === computerCount) {
    document.querySelector(".show-winner").textContent = "Боротьба була на рівних!";
  } else {
    document.querySelector(".show-winner").textContent = "Пощастить іншого разу!";
  }

}

function startGame() {
  clearInterval(levelInterval);
  countArr.splice(0, 100);
  for (let i = 0; i < 100; i++) {
    document.getElementById(`td-${i+1}`).removeAttribute("class");
  }
  playerCount = 0;
  computerCount = 0;
  document.getElementById("computer-count").innerText = 0;
  document.getElementById("player-count").innerText = 0;
  document.querySelector(".show-winner").textContent = '';
}


